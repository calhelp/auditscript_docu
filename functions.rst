﻿=================================================================
README
=================================================================

Global Debug:

    The variable _auditscript_DEBUG_MODE Can be accessed by scripts and is either true or false depending on the value of the GUI dropdown menu or specified
    as a runtime argument.

Argument rules:
	Numeric arguments: 
	- Quotation marks donâ€™t matter as long as string can be converted to number.
	- Python variables, strings, or numbers interchangeable.	

	Non numeric variable arguments:
	- Quotations required to distinguish strings from python variables. 
	- â€œSome stringâ€ or some_python_var are both acceptable inputs.

	Type arguments:
	- Quotations donâ€™t matter.


**ACC:**

   Macro for settings and retrieving measurement values.

.. py:function:: ACC(alias; variable_name)

   Retrieves the value of the named variable for the given alias.

   :param str alias: Alias.
   :param str variable_name: Name of variable to retrieve.
   :return: Value of variable specified.
   :rtype: str or int
   :raises MacroError: if the alias or variable name don't exist.

.. py:function:: ACC(alias; range; result; unit; accuracy; tolerance)

   Stores a results value and its attributes under the specified alias.

   :param str alias: Alias.
   :param int range: Range of measurement.
   :param int result: Value of measurement.
   :param str unit: Unit of measurement value.
   :param float accuracy: Accuracy of measurement.
   :param float tolerance: Tolerance of measurement.


**BEEP:**

   Macro for generating warning sounds.

.. py:function:: BEEP()

   Plays a default beep sound using the systems built in speaker.

.. py:function:: BEEP(sound_file)

   Plays the provided sound file with the systems external speakers.

   :param str sound_file: Path to sound file.

.. py:function:: BEEP(frequency; duration)

   Plays a beep sound with the provided frequency (hertz) and duration (milliseconds) with the systems external speakers.

   :param int frequency: Frequency of beep sound in hertz.
   :param int duration: Duration of sound in milliseconds.


**CALL:**

   Macro for importing scripts.

.. py:function:: CALL(script_path)

   Parses a script from another file inline at location of call. The path argument must must not contain quotes and
   cannot be a python variable.

   :param str script_path: Path to script being imported.


**COMPARE:**

   Macro for storing and comparing measurement values.

.. py:function:: COMPARE(db_alias; alias_uut; alias_std)

   Stores the results of the current measurement in the specified database as is.

   :param str db_alias: Alias of database used for storage.
   :param str alias_uut: Alias of UUT measurement to be stored.
   :param str alias_std: Alias of STD measurement to be stored.

.. py:function:: COMPARE(db_alias; alias_uut; alias_std; type; config)
	
   Calculates a result and stores it in the provided database. Valid compare
   types are NOMINAL or READING. Config string format is any combination of
   "+-(percent)% +-(range_percent)/ +-(offset)U" where + and - signs are optional.

   :param str db_alias: Alias of database used for storage.
   :param str alias_uut: Alias of UUT measurement to be stored.
   :param str alias_std: Alias of STD measurement to be stored.
   :param str type: String representing the type of compare to be done.
   :param str config: Config string in format "+-(percent)% +-(range_percent)/ +-(offset)U"

   example:

        "0.1% 0.003/ 0.003U" uses values for both min and max valid ranges.
        same as "+0.1% -0.1% +0.003/ -0.003/ +0.003U -0.003U"
        any combination of + and - values are allowed to specify config.
	         
        "0.1% 0.03/ 0.2U" -> tol_min = value - (1 * 0.1 / 100) - range * ( 1 + 0.03 / 100) - 0.2
	         	
	    tol_max  = value + (1 * 0.1 / 100) + range * ( 1 + 0.03 / 100) + 0.2
	         
	NOMINAL calculation is as follows: 
	    result = uut_value_tol_min <= std_value <= uut_value_tol_max
		
	READING calculation is as follows:
		result = uut_value_tol_min <= uut_value <= uut_value_tol_max
		
	Both NOMINAL and READING mode also calculate an error value:
		error = (uut_value - std_value)/std_value * 100
		
	Note: The order of the alias is used to determine which one is the uut alias
	and which one is the std alias. The first alias being std and second being
	uut.


**CONFIG:**

   Macro for configuring devices and databases.

.. py:function:: CONFIG(alias; db_type; path_to_db)
	
   Configures a database file for storing data. Valid types are SQLite and
   CSV. If the file specified already exists, new measurements will be appended
   to the file, otherwise a new file is created.

   :param str alias: Alias.
   :param str db_type: Type of database to configure.
   :param str path_to_db: Path to database file.

.. py:function:: CONFIG(alias; device_type; device_port)
	
   Configures a new preset device for the specified alias connected to the
   provided port. Current presets: FLUKE289, FILE.
	
   :param str alias: Alias.
   :param str device_type: Type of device.
   :param str device_port: Port of device.


**MEAS:**

.. py:function:: MEAS(DESCRIPTION; description)
	
   Sets the current measurements description string. If str is not provided
   returns the current measurements description string.

   ::param str description: Measurement description.

.. py:function:: MEAS(HEADER; header)
	
   Sets the current measurements header string. If str is not provided
   returns the current measurements header string.

   :param str header: Measurement header.
	
.. py:function:: MEAS(RESULT; result)
	
   Sets the current measurements result string. If str is not provided
   returns the current measurements result string.

   :param str result: Measurement result.

.. py:function:: MEAS(UNC; unc)
	
   Sets the current measurements UNC string. If str is not provided returns
   the value of the current UNC string.

   :param str unc: Measurement unc.

.. py:function:: MEAS(*)

   Clears all measurement data, including anything stored in value aliases.
   Value aliases themselves remain along with their names.


**MESSAGE:**

    Macro for displaying dialog boxes.

.. py:function:: MESSAGE(INFO; msg; [path_to_image])
	
   Displays the contents of msg in a INFO style dialog box. In console mode
   prints the msg to the console. Optional image argument displays an image.

   :param str msg: Message to be shown.
   :param str path_to_image: Path to an image that will be displayed in dialog box.
	
.. py:function:: MESSAGE(ERROR; msg)
	
   Displays the contents of msg in a ERROR style dialog box. In console mode
   prints the msg to the console. Script execution is canceled after user
   selects OK.

   :param str msg: Message to be shown.

.. py:function:: MESSAGE(PROMPT; msg; result; [path_to_image])
	
   Displays a PROMPT style dialog box with message specified by msg allowing
   the user to select either Ok or Cancel to set the result variable to True or
   False. path_to_image is an optional argument allowing an image to be shown
   above the msg. For console mode, asks user to enter yes/no, ignores image.

   :param str msg: Message to be show.
   :param str result: Name of variable used to store result.
   :param str path_to_image: Path to an image that will be displayed in dialog box.
	
.. py:function:: MESSAGE(INPUT; msg; result; type; [path_to_image])
	
   Displays an INPUT style dialog box with message specified by msg with a text
   box allowing the user to submit data. Valid types are STRING, DOUBLE and
   INTEGER allowing the input box to validate that the correct data format is
   used. User input is stored in the result variable. path_to_image is an
   optional argument allowing an image to be shown above the msg. Console mode
   prompts the user to enter input via console, ignoring the image file.

   :param str msg: Message to be shown.
   :param str result: Name of variable used to store result.
   :param str type: Type of result.
   :param str path_to_image: Path to an image that will be displayed in dialog box.

.. py:function:: MESSAGE(DEVICES; alias)

   Brings up the device select dialog allowing the user to select form a list of connected
   devices. After the user selects a device it is stored in the provided alias.

   :param str alias: Alias that will be used to store selected device.

.. py:function:: MESSAGE(COMBO; msg; selection_list; result)

   Creates a combobox dialog where the user can select an option from the provided list of options.
   Returns string of selection.

   :param str msg: Message to be shown.
   :param list selection_list: List of options to be shown in combobox.
   :param str result: Name of variable used to store result.

.. py:function MESSAGE(TIMER; msg; time_in_seconds)

   Creates a timer dialog. The user is unable to proceed until the provided number of seconds have elapsed.

   :param str msg: Message to be shown.
   :param int time_in_seconds: Time in seconds.


**RESULT:**

   Macro for writing values to database.

.. py:function:: RESULT(db_alias; uut_alias; std_alias)
	
   Writes a result to the database referenced by db_alias. uut_alias and
   std_alias are optional. To specify an std_alias without a uut_alias you must
   pass a None value for the uut_alias.

   :param str db_alias: Alias of database used for storage.
   :param str alias_uut: Alias of UUT measurement to be stored.
   :param str alias_std: Alias of STD measurement to be stored.
	
	Example:
		``Result('db_alias'; None; 'std_alias')``
	
	
**TALK:**

   Macro for communicating with devices.

.. py:function:: TALK(alias; msg)
	
   Sends contents of msg to device configured under alias. Throws MacroError
   if no device has been configured or alias does not exist.

   :param str alias: Alias of device to communicate with.
   :param str msg: Message to send to device.

.. py:function:: TALK(alias; msg; reply; delay)

   Sends contents of msg to device configured under alias after waiting for a
   number of milliseconds specified by delay and stores response in the reply
   variable.

   :param str alias: Alias of device to communicate with.
   :param str msg: Message to send to device.
   :param str reply: Name of variable to store reply.


**WAIT:**

.. py:function:: WAIT(time)
	
   Causes the script to wait t milliseconds before continuing.

   :param int time: Time in ms to wait.
	