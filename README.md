.. image:: |version-badge| http://img.shields.io/badge/VERSION-1.6.2-green.svg 	  
.. image:: |copyright-badge| http://img.shields.io/badge/COPYRIGHT-calHelp-orange.svg 



AuditScript Documentation
=========================

**AuditScript** provides a Macro language for automatic measurement scripts. 
It should help technicans building automaticly measurement scripts like calibration procedures or test and validation procedures 
with easy to use commands. **AuditScript** is a complete environment for controlling devices, message handling and secure saving 
the results. 



Description
------------

Reads a text file with macros as described in "Macro Workflow.pdf" and converts 
it into python before executing the script.


Installation (for non USB-Stick version)
-----------------------------------------

..  note:: no installation required 

Requires:
	- pyvisa ("https://github.com/hgrecco/pyvisa") to interface with devices.
	- wxPython ("http://www.wxpython.org/download.php") for the gui which will need to be downloaded from the site and installed via installer.
	 
	 
.. warning:: **The System required installed NI-VISA hardware driver for communicating with controlled devices.**
	 

Command line version usage
---------------------------

from cmd navigate to dir containing auditscript_run.py and run::
	"python auditscript_run.py --no-gui"
	
		 
optional arguments::
- simulation_mode
When passed measure_macros will use a mock version of the pyVisa
library that will return random results without requiring a device
connection.
		
- input
	Input script text file containing macros.
		
- no_gui
	Will run without gui

- debug_mode
	sets the value of the _auditscript_DEBUG_MODE script variable.

- visa_library
	Path to visa library.

default argument values can be configured in auditscript/config.py