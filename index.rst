.. auditscript documentation master file, created by
   sphinx-quickstart on Fri Jul 25 17:56:30 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to auditscript's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 2

   dialog.rst
   calculate.rst
   device.rst
   measurement.rst
   databases.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

