.. image:: http://img.shields.io/badge/VERSION-1.6.2-green.svg   
.. image:: http://img.shields.io/badge/COPYRIGHT-calHelp-orange.svg 




=================================================================
AuditScript Documentation
=================================================================

**AuditScript** provides a Macro language for automatic measurement scripts.


.. note:: **The System required installed NI-VISA hardware driver for communicating with controlled devices.**

  

Installation
----------------

..  note:: no installation required 



Getting Started
----------------

To get started, try the :doc:`README`, or for complete documentation, 
check out the :doc:`AuditScript` documentation page.


Documentation Contents:
--------------------------

.. toctree::
   :maxdepth: 2

   README.md

   Functions
   Samples.txt
   Version

   LICENSE
   


Indices and tables
-------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Version
----------

.. image:: http://img.shields.io/badge/VERSION-[version]-green.svg   

This documentation is for AuditScript |version|.


=================================================================
Functions
=================================================================

``Argument rules:``
------------------
	
	Numeric arguments: 
	- Quotation marks dont matter as long as string can be converted to number.
	- Python variables, strings, or numbers interchangeable.	

	Non numeric variable arguments:
	- Quotations required to distinguish strings from python variables. 
	- Some string or some_python_var are both acceptable inputs.

	Type arguments:
	- Quotations dont matter.
	
	.. warning:: For special characters ...


``MyMacro``
------------------

	.. note:: Send a message to a recipient
	.. py:function:: send_message(sender, recipient, message_body, [priority=1])
	:param str sender: The person sending the message
	:param str recipient: The recipient of the message
	:param str message_body: The body of the message
	:param priority: The priority of the message, can be a number 1-5
	:type priority: integer or None
	:return: the message id
	:rtype: int
	:raises ValueError: if the message_body exceeds 160 characters
	:raises TypeError: if the message_body is not a basestring
   
	Default: ``MyMacro(alias; variable_name)``   


	here the sample::
	if MyMacro() == true:
		MESSAGE(INFO; "MyMacro is true")
		#this will make a message


``Global Debug:``
------------------

    .. note:: The variable _auditscript_DEBUG_MODE Can be accessed by scripts and is either true or false depending on the value of the GUI dropdown menu or specified
    as a runtime argument.
	

here the sample::

	if _auditscript_DEBUG_MODE == true:
		MESSAGE(INFO; "Debug mode is running.")
		#this will make a message


``ACC:``
------------------
	.. py:function:: ACC(alias; range; result; unit; accuracy; tolerance)
	
	.. note:: Stores a result value and its attributes under the specified alias.

   :param str alias: The person sending the message
   :param str range: The recipient of the message
   :param str result: The body of the message
   :param priority: The priority of the message, can be a number 1-5
   :type priority: integer or None
   :return: the message id
   :rtype: int
   :raises ValueError: if the message_body exceeds 160 characters
   :raises TypeError: if the message_body is not a basestring
   
	``ACC(alias; variable_name)``
	
	Retrieves the value of the named variable for the given alias. Throws a
	Macro exception if the alias does not exist. 
	

here the sample::

	CONFIG("ALIAS_DB"; SQLite; "test.db") # configure new test database
	CONFIG("Alias_STD"; RS232; COM1 ; "9600,1,8,N"; "CRLF") # configure device 1
	MEAS(HEADER; "10 V DC Measurement")
	TALK("Alias_UUT"; "OUT 10.00V")
	MESSAGE(INPUT; "What is the measurement value?"; UUT_ANSWER, DOUBLE)
	ACC("Alias_STD"; 10; 10.00; "V"; 0.0001; "0.001% 0.004U")
	ACC("Alias_UUT"; 10; UUT_ANSWER; "V"; 0.01; "0.1% 0.02U")

	COMPARE("ALIAS_DB"; "Alias_STD"; "Alias_UUT"; READING)


CALL:
    ``CALL(path)``

    Parses a script from another file inline at location of call. The path argument must must not contain quotes and
    cannot be a python variable.


COMPARE:
	``COMPARE(db_alias; alias_uut; alias_std)``
	
	Stores the results of the current measurement as is.
	
	
	``COMPARE(db_alias; alias_uut; alias_std; type; config)``
	
	Calculates a result and stores it in the provided database. Valid compare 
	types are NOMINAL or READING. Config string format is any combination of
	"+-(percent)% +-(range_percent)/ +-(offset)U" where + and - signs are optional.
	
	example:
	    "0.1% 0.003/ 0.003U" uses values for both min and max valid ranges.
	    same as "+0.1% -0.1% +0.003/ -0.003/ +0.003U -0.003U"
	    any combination of + and - values are allowed to specify config.
	         
	    "0.1% 0.03/ 0.2U" -> tol_min = value - (1 * 0.1 / 100) - range * ( 1 + 0.03 / 100) - 0.2
	         	
	    tol_max  = value + (1 * 0.1 / 100) + range * ( 1 + 0.03 / 100) + 0.2
	         
	NOMINAL calculation is as follows: 
		result = uut_value_tol_min <= std_value <= uut_value_tol_max
		
	READING calculation is as follows:
		result = uut_value_tol_min <= uut_value <= uut_value_tol_max
		
	Both NOMINAL and READING mode also calculate an error value:
		error = (uut_value - std_value)/std_value * 100
		
	Note: The order of the alias is used to determine which one is the uut alias
	and which one is the std alias. The first alias being std and second being
	uut.


CONFIG:
	``CONFIG(alias; db_type; path_to_db)``
	
	Configures a database file for storing data. Valid types are SQLite and 
	CSV. If the file specified already exists, new measurements will be appended 
	to the file, otherwise a new file is created.
	
	
	``CONFIG(alias; device_type; device_port)``
	
	Configures a new preset device for the specified alias connected to the
	provided port. Current presets: FLUKE289, FILE.
	
	
	``CONFIG(alias; RS232; device_port; config; term_chars)``
	
	Configures a new RS232 device. "config" is a string with format
	"baud_rate, stop_bits, data_bits, parity". Termination chars can be specified
	with term_chars as "LF" "CR" or "CRLF".


	``CONFIG(alias; GPIB; address; board_number)``

	Configures a new GPIB device specified address. board_number argument is optional and defaults to 0.


	``CONFIG(alias; TCPIP; address; port)``
	or
    ``CONFIG(alias; TCPIP; address)``

	Configures a new TCPIP device. Address is the IPv4 IP address of device. Port is the TCP port.


	``CONFIG(alias; USB; port)``

	Configures a new USB device connected to the specified USB port.
	

MEAS:
	``MEAS(DESCRIPTION; str)``
	
	Sets the current measurements description string. If str is not provided
	returns the current measurements description string.
	
	
	``MEAS(HEADER; str)``
	
	Sets the current measurements header string. If str is not provided
	returns the current measurements header string.
	
	
	``MEAS(RESULT; str)``
	
	Sets the current measurements result string. If str is not provided
	returns the current measurements result string.
	
	
	``MEAS(UNC; str)``
	
	Sets the current measurements UNC string. If str is not provided returns
	the value of the current UNC string.
	
	
	``MEAS(*)``
	Clears all measurement data, including anything stored in value aliases. 
	Value aliases themselves remain along with their names.


MESSAGE:
	``MESSAGE(INFO; msg; path_to_image)``
	
	Displays the contents of msg in a INFO style dialog box. In console mode
	prints the msg to the console. Optional image argument displays an image.
	
	
	``MESSAGE(ERROR; msg)``
	
	Displays the contents of msg in a ERROR style dialog box. In console mode
	prints the msg to the console. Script execution is canceled after user
	selects OK.
	
	
	``MESSAGE(PROMPT; msg; result; path_to_image)``
	
	Displays a PROMPT style dialog box with message specified by msg allowing
	the user to select either Ok or Cancel to set the result variable to True or
	False. path_to_image is an optional argument allowing an image to be shown
	above the msg. For console mode, asks user to enter yes/no, ignores image.
	
	
	``MESSAGE(INPUT; msg; result; type; path_to_image)``
	
	Displays an INPUT style dialog box with message specified by msg with a text
	box allowing the user to submit data. Valid types are STRING, DOUBLE and
	INTEGER allowing the input box to validate that the correct data format is
	used. User input is stored in the result variable. path_to_image is an
	optional argument allowing an image to be shown above the msg. Console mode
	prompts the user to enter input via console, ignoring the image file.


	``MESSAGE(DEVICES; alias)``

	Brings up the device select dialog allowing the user to select form a list of connected
	devices. After the user selects a device it is stored in the provided alias.


	``MESSAGE(COMBO; message; selection_list; result)``

	Creates a combobox dialog where the user can select an option from the provided list of options.
	Returns string of selection.


	``MESSAGE(TIMER; message; time_in_seconds)``

	Creates a timer dialog. The user is unable to proceed until the provided number of seconds have elapsed.
	
	
RESULT:
	``RESULT(db_alias; uut_alias; std_alias)``
	
	Writes a result to the database referenced by db_alias. uut_alias and 
	std_alias are optional. To specify an std_alias without a uut_alias you must
	pass a None value for the uut_alias. 
	
	Example:
		``Result('db_alias'; None; 'std_alias')``
	
	
TALK:
	``TALK(alias; msg)``
	
	Sends contents of msg to device configured under alias. Throws MacroError
	if no device has been configured or alias does not exist.
	
	
	TALK(alias; msg; reply; delay)
	
	Sends contents of msg to device configured under alias after waiting for a 
	number of milliseconds specified by delay and stores response in the reply
	variable. 
	

WAIT:
	``WAIT(t)``
	
	Causes the script to wait t milliseconds before continuing. 
	
Configuration file
=======================================================================

There is an config.py available to configure the main start and running states::

	#default configuration options
	
	visa_library_path = ''
	debug_mode = True
	simulation_mode = True
	parser_output = 'auditscript/parser_generated_scipt.py'
	default_script = 'scripts/test_script_2.txt'
	
	
	# Grid View
	# default configuration options
	visa_library_path = None
	debug_mode = True
	simulation_mode = True
	parser_output = 'parser_generated_scipt.py'
	default_script = 'scripts/macro_tests/combobox_and_timer.txt'
	
	# name of database table were measurements are stored
	>>>>>>> cf4e1fd1926277e9f3c7ba7e9f8c2cc37a2bbae9
	database_table_name = 'measurements'
	
	# can be tuple (height, width) eg. main_window_default_size = (400, 500)
	main_window_default_size = 'maximized'
	
	# Values recorded in database in order.
	database_columns = [
		'step',
		'line_number',
		'header',
		'description',
		'unc',
		'uut_name',
		'uut_value_range',
		'uut_value',
		'uut_tolerance',
		'uut_unit',
		'uut_resolution',
		'std_name',
		'std_value_range',
		'std_value',
		'std_tolerance',
		'std_unit',
		'std_resolution',
		'result',
		'tol_ref',
		'tol_neg',
		'tol_pos',
		'tol_err',
	]
	
	# small individual German sample.
	database_columns_de = [
		'step',
		'header',
		'uut_value_range',
		'uut_value',
		'std_value',
		'uut_unit',
		'uut_tolerance',
		'tol_err',
		'unc',
		'result',
	]
	
	# spaces are replaced with '_' for sqlLite databases.
	database_column_names = {
		'step': 'Step',
		'line_number': 'Line Number',
		'header': 'Header',
		'description': 'Description',
		'unc': 'UNC',
		'uut_name': 'UUT Name',
		'uut_value_range': 'UUT Value Range',
		'uut_value': 'UUT Value',
		'uut_tolerance': 'UUT Tolerance',
		'uut_unit': 'UUT Unit',
		'uut_resolution': 'UUT Resolution',
		'std_name': 'STD Name',
		'std_value_range': 'STD Value Range',
		'std_value': 'STD Value',
		'std_tolerance': 'STD Tolerance',
		'std_unit': 'STD Unit',
		'std_resolution': 'STD Resolution',
		'result': 'Result',
		'tol_ref': 'Tol Ref',
		'tol_neg': 'Tol Neg',
		'tol_pos': 'Tol Pos',
		'tol_err': 'Tol Err',
	}
	
	database_column_names_de = {
		'step': 'Schritt',
		'line_number': 'Zeilennummer',
		'header': 'Messung',
		'description': 'Beschreibung',
		'unc': 'Unsicherheit',
		'uut_name': 'UUT Name',
		'uut_value_range': 'Bereich',
		'uut_value': 'Messwert',
		'uut_tolerance': 'zul. Toleranz',
		'uut_unit': 'UUT Einheit',
		'uut_resolution': 'UUT Auflösung',
		'std_name': 'STD Name',
		'std_value_range': 'STD Bereich',
		'std_value': 'STD Value',
		'std_tolerance': 'STD Genauigkeit',
		'std_unit': 'STD Einheit',
		'std_resolution': 'STD Auflösung',
		'result': 'Ergebnis',
		'tol_ref': 'Tol Ref',
		'tol_neg': 'Tol Neg',
		'tol_pos': 'Tol Pos',
		'tol_err': 'Toleranzauslastung',
	}
	
	=======
	# grid result coloring
	grid_display_settings = {
		'result': {'pass': 'green', 'fail': 'red'},
	}
	
	<<<<<<< HEAD
	grid_columns = database_columns_de
	grid_column_names = database_column_names_de
	=======
	# list of columns to display in the grid view
	grid_columns = database_columns
	
	# dictionary of column names for grid view header
	grid_column_names = database_column_names



..  note:: The ``config.py`` file is located under path /auditscript.


	
=================================================================
Readme
=================================================================
.. include:: README.md
	
=================================================================
License
=================================================================
.. include:: LICENSE.txt
    :literal:
